import React from 'react'

import { Form, Input, Button, Select } from 'antd'
import { getConfigure, setConfigure } from './api/api'

const { Option } = Select

const layout = {
  labelCol: {
    span: 4,
  },
  wrapperCol: {
    span: 16,
  },
}
const tailLayout = {
  wrapperCol: {
    offset: 4,
    span: 16,
  },
}

export default class Configure extends React.Component {
  formRef = React.createRef()

  getInfo = () => {
    getConfigure().then(response => {
      const { data } = response
      this.formRef.current.setFieldsValue({
        appId: data.appId,
        appSecret: data.appSecret,
        region: data.region,
        tableName: data.tableName
      })
    })
  }

  constructor(props) {
    super(props)
    this.onFinish = this.onFinish.bind(this)
  }


  onFinish(values) {
    setConfigure(values).then(() => {

    }).catch(() => {
      this.getInfo()
    })
  }

  onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo)
  }

  componentDidMount() {
    this.getInfo()
  }

  render() {
    return (
      <Form
        {...layout}
        name="basic"
        ref={this.formRef}
        onFinish={this.onFinish}
        onFinishFailed={this.onFinishFailed}
      >
        <Form.Item
          label="Aws AppId"
          name="appId"
          rules={[
            {
              required: true,
              message: 'Please input AWS AppId!',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Aws AppSecret"
          name="appSecret"
          rules={[
            {
              required: true,
              message: 'Please input AWS AppSecret!',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="region"
          label="Aws Region"
          rules={[
            {
              required: true,
            },
          ]}
        >
          <Select
            placeholder="Select a aws region"
            allowClear
          >
            <Option value="ap-south-1">ap-south-1</Option>
            <Option value="eu-north-1">eu-north-1</Option>
            <Option value="eu-west-3">eu-west-3</Option>
            <Option value="eu-west-2">eu-west-2</Option>
            <Option value="eu-west-1">eu-west-1</Option>
            <Option value="ap-northeast-2">ap-northeast-2</Option>
            <Option value="ap-northeast-1">ap-northeast-1</Option>
            <Option value="me-south-1">me-south-1</Option>
            <Option value="us-gov-east-1">us-gov-east-1</Option>
            <Option value="ca-central-1">ca-central-1</Option>
            <Option value="ap-east-1">ap-east-1</Option>
            <Option value="cn-north-1">cn-north-1</Option>
            <Option value="us-gov-west-1">us-gov-west-1</Option>
            <Option value="ap-southeast-1">ap-southeast-1</Option>
            <Option value="ap-southeast-2">ap-southeast-2</Option>
            <Option value="us-iso-east-1">us-iso-east-1</Option>
            <Option value="eu-central-1">eu-central-1</Option>
            <Option value="us-east-1">us-east-1</Option>
            <Option value="us-east-2">us-east-2</Option>
            <Option value="us-west-1">us-west-1</Option>
            <Option value="cn-northwest-1">cn-northwest-1</Option>
            <Option value="us-isob-east-1">us-isob-east-1</Option>
            <Option value="us-west-2">us-west-2</Option>
          </Select>
        </Form.Item>

        <Form.Item
          label="Table Name"
          name="tableName"
          rules={[
            {
              required: true,
              message: 'Please input Table Name!',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit">
            Save
          </Button>
        </Form.Item>
      </Form>
    )
  }
}
