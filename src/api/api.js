import request from '../utils/request'

/**
 * send post request
 * @param url
 * @param data
 * @returns {AxiosPromise}
 */
export function post(url, data) {
  return request({
    url: url,
    method: 'post',
    data
  })
}

export const getConfigure = () => {
  return post('/configure/get')
}

export const setConfigure = (data) => {
  return post('/configure/set', data)
}

export const searchLog = (data) => {
  return post('/search', data)
}
