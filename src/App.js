import React from 'react'
import { Layout, Menu } from 'antd'

import './App.css'
import Configure from './Configure'
import BuildLog from './BuildLog'

import {
  DesktopOutlined,
  EditOutlined,
} from '@ant-design/icons'

const { Header, Content } = Layout

class App extends React.Component {
  state = {
    current: 'main',
  }

  handleClick = e => {
    console.log('click ', e)
    this.setState({
      current: e.key,
    })
  }

  render() {
    return (
      <Layout>
        <Header style={{ position: 'fixed', zIndex: 1, width: '100%' }}>
          <div className="logo" />
          <Menu
            theme="dark"
            mode="horizontal"
            style={{ lineHeight: '64px' }}
            onClick={this.handleClick} selectedKeys={[this.state.current]}
          >
            <Menu.Item key="main">
              <DesktopOutlined />
              Search build data
            </Menu.Item>
            <Menu.Item key="config">
              <EditOutlined />
              AWS configuration
            </Menu.Item>
          </Menu>
        </Header>
        <Content className="layout-content" style={{ padding: '0 50px', marginTop: 100 }}>
          <div style={{ display: this.state.current === 'main' ? 'block' : 'none' }}>
            <BuildLog />
          </div>
          <div style={{ display: this.state.current !== 'main' ? 'block' : 'none' }}>
            <Configure />
          </div>
        </Content>
      </Layout>
    )
  }
}

export default App
