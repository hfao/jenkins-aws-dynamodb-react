import axios from 'axios'
import { message } from 'antd'
import Qs from 'qs'

// create an axios instance
const service = axios.create({
  baseURL: 'http://jenkins.api.fabrice.cn', // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 50000, // request timeout
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
  },
  transformRequest: [
    function(data) {
      // 在请求之前对data传参进行格式转换
      return Qs.stringify(data)
    }
  ]
})

// response interceptor
service.interceptors.response.use(
  response => {
    const res = response.data
    // if the custom code is not 20000, it is judged as an error.
    if (res.code !== 0) {
      message.error(res.msg || 'Error')
      return Promise.reject(new Error(res.msg || 'Error'))
    } else {
      return res
    }
  },
  error => {
    console.log('err' + error) // for debug
    message.error('Error')
    return Promise.reject(error)
  }
)
export default service
