import React from 'react'
import { Button, Col, DatePicker, Form, Input, Row, Table } from 'antd'

import './BuildLog.css'
import { searchLog } from './api/api'

const { RangePicker } = DatePicker

const columns = [
  {
    title: 'JobName',
    dataIndex: 'jobName',
    key: 'jobName'
  },
  {
    title: 'BuildNumber',
    dataIndex: 'buildNumber',
    key: 'buildNumber',
  },
  {
    title: 'BuildTime',
    dataIndex: 'buildTime',
    key: 'buildTime',
  },
  {
    title: 'ConsoleLog',
    dataIndex: 'consoleLog',
    key: 'consoleLog',
    ellipsis: true
  }
]

const expandable = { expandedRowRender: record => <p style={{ whiteSpace: 'pre-wrap' }}>{record['consoleLog']}</p> }

export default class BuildLog extends React.Component {
  formRef = React.createRef()

  constructor(props) {
    super(props)
    this.state = {
      table: {
        loading: false,
        list: []
      }
    }
  }

  componentDidMount() {
    this.getList()
  }

  getList = (data) => {
    this.setState({
      table: {
        loading: true
      }
    })
    searchLog(data).then(response => {
      const resData = response.data
      console.log('resData', resData)
      this.setState({
        table: {
          loading: false,
          list: response.data
        }
      })
    })
  }

  onFinish = values => {
    console.log('Received values of form: ', values)
    let jobName, content, beginDateTime, endDateTime
    jobName = values['jobName'] !== undefined ? values['jobName'] : ''
    content = values['content'] !== undefined ? values['content'] : ''
    if (values['dateTimeRange'] === undefined || values['dateTimeRange'] === null) {
      beginDateTime = ''
      endDateTime = ''
    } else {
      beginDateTime = values['dateTimeRange'][0].format('YYYY-MM-DD HH:mm:ss')
      endDateTime = values['dateTimeRange'][1].format('YYYY-MM-DD HH:mm:ss')
    }
    const data = {
      jobName: jobName,
      content: content,
      beginDateTime: beginDateTime,
      endDateTime: endDateTime
    }
    this.getList(data)
  }

  resetForm = () => {
    this.formRef.current.resetFields()
    this.getList()
  }

  render() {
    return (
      <div>
        <Form
          ref={this.formRef}
          name="advanced_search"
          className="ant-advanced-search-form"
          onFinish={this.onFinish}
        >
          <Row gutter={24}>
            <Col span={8}>
              <Form.Item
                name="jobName"
                label="Job Name"
              >
                <Input placeholder="input job name to search" />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item name="dateTimeRange" label="DateRange">
                <RangePicker showTime format="YYYY-MM-DD HH:mm:ss" />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item
                name="content"
                label="Log Content"
              >
                <Input placeholder="input log content to search" />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col
              span={24}
              style={{
                textAlign: 'right',
              }}
            >
              <Button type="primary" htmlType="submit">
                Search
              </Button>
              <Button
                style={{
                  marginLeft: 8,
                }}
                onClick={this.resetForm}
              >
                Clear
              </Button>
            </Col>
          </Row>
        </Form>
        <div className="search-result-list">
          <Table expandable={expandable} rowKey="id" columns={columns} dataSource={this.state.table.list}
                 loading={this.state.table.loading} />
        </div>
      </div>
    )
  }
}
